Pod::Spec.new do |s|
  s.name         = "HelloFooBar"
  s.version      = "0.0.1"
  s.summary      = "HelloCocoaPods is a hello world framework."
  s.description  = <<-DESC
                    HelloCocoaPods may be useful when you learn about "Swift Framework".
                   DESC
  s.homepage     = "https://bitbucket.org/saber_chica/hellococoapods"
  s.license      = { :type => "MIT", :file => "LICENSE" }
  s.author       = { "tomoko" => "saber.chica@gmail.com" }
  s.source       = { :git => "https://bitbucket.org/saber_chica/hello-cocoapos.git", :tag => "#{s.version}" }

  s.ios.deployment_target = "9.0"

  s.source_files = "HelloCocoaPods/*.{h,swift}"
  s.exclude_files = "HelloCocoaPodsTests"
end
