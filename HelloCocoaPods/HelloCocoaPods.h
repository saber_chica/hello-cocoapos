//
//  HelloCocoaPods.h
//  HelloCocoaPods
//
//  Created by Tomoko Sohda on 6/6/16.
//  Copyright © 2016 Tomoko Sohda. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for HelloCocoaPods.
FOUNDATION_EXPORT double HelloCocoaPodsVersionNumber;

//! Project version string for HelloCocoaPods.
FOUNDATION_EXPORT const unsigned char HelloCocoaPodsVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <HelloCocoaPods/PublicHeader.h>
