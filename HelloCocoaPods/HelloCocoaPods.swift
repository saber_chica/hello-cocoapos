//
//  HelloCocoaPods.swift
//  HelloCocoaPods
//
//  Created by Tomoko Sohda on 6/6/16.
//  Copyright © 2016 Tomoko Sohda. All rights reserved.
//

import Foundation

public class HelloCocoaPods {
    public init(){
    }
    
    public func message() -> String {
        let message = "hello CocoaPods."
        print(message)
        return message
    }
}
